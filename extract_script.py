"""This is a simple script to extract links out of Dance Music Lovers
Kenya :) facebook group.

For compatibility ensure requests module is up to date. json method did not exist
in older versions.
"""
from contextlib import closing
import re
import requests
import urllib


GRAPH_URL = "https://graph.facebook.com/514078905336061/feed"


# Access tokens are temporary and will need to be regenerated and reused each 
# time the script is to be used. Get one from https://developers.facebook.com/tools/explorer
# get access for `user_groups` and `user_friends`
ACCESS_TOKEN = 'CAACEdEose0cBAFDxqGZBrP9u31ZBXSYYQ1hRp0zpFgiRuetBTY9IVihpGulFOnWdg6uvJbJyCe7MBfYFPZACSQjycPSbzKMgMKz56fPZBVcS3YDPmjbuVRjklTeMlkVxZB7qqYFr0GwgblpiiVV47Jy8QEEufaUmInHZC1uXGZCsRzr5KhH4LCIH7ARKD4sR8BI8I197McMvgZDZD'


#Set this to directory to store the file
LINK_STORAGE_FILE_PATH = '/home/ny2ko/workspace/url_extractor'


def get_batch(url):
    print url
    r = requests.get(url)
    return r.json()

def cleanse_url(url):
    """Reformats url so it is usable with roll.io

    Currently this caters only for youtube links which can come
    up in a variety of ways. They should simply be of the form
    `http://www.youtube.com/watch?v=`
    """
    if 'youtube.com' in url:
        print url
        youtube_id = re.search(r'v=[^&]*', url)
        cleansed_url = 'http://www.youtube.com/watch?{0}'.format(youtube_id.group(0))
        return cleansed_url
    return url

def extract_links(json):
    links = []
    for post in json:
        if 'link' in post:
            # Decode any url_encoded urls. TODO: Some posts have more than for link. 
            # Need to filter out only relevant ones.
            url = urllib.unquote(post['link'])
            links.append(cleanse_url(url))
    return links

def get_dance_music_links():
    all_links= []
    initial = GRAPH_URL + '?access_token=' + ACCESS_TOKEN
    response = get_batch(initial)
    to_add = extract_links(response['data'])
    all_links.extend(to_add)

    # While loop to go through all pages of the wall
    while 'paging' in response:
        if 'next' in response['paging']:
            next_url = response['paging']['next']
            response = get_batch(next_url)
            all_links.extend(extract_links(response['data']))

    # Write out the links to a file. TODO: Change to upload to roll.io automatically
    output = LINK_STORAGE_FILE_PATH + '/links.txt'
    print all_links
    with closing(open(output, 'w')) as f:
        f.write('\n'.join(all_links))


if __name__ == '__main__':
    get_dance_music_links()
